var imageUri;
var inputValue;
var imageDataArray=[];

function titleClick() {
	document.getElementById("inputBoxBeforeClick").style.display = "none";
	document.getElementById("inputBoxAfterClick").style.display = "block";
}

function changeTitleClick() {
	document.getElementById("inputBoxBeforeClick").style.display = "table";
	document.getElementById("inputBoxAfterClick").style.display = "none";
}

function clearElements() {
	document.getElementById("inputElement").value="";
	var element = document.getElementById("pictureBuffer");
	if(element.hasChildNodes())
	{
		var picture=document.getElementById("myspan");
		element.removeChild(picture);
	}
}

function handleFileSelect(evt) {
	
    var files = evt.target.files; // FileList object
	
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // // Only process image files.
      // if (!f.type.match('image.*')) {
        // continue;
      // }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
		  span.id="myspan"
          span.innerHTML = ['<img class="thumb" id="myImage" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		 			
		document.getElementById("pictureBuffer").appendChild(span);
		
        };
		})(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }
  
function addPicture() {
	//console.log(document.getElementById("files"));
	document.getElementById("files").addEventListener('change', handleFileSelect, false);
	}
	
	
function initializeDiv(maxWidth,minHeight)
{
		var divElement = document.createElement("Div");
														//Created a div Element to disply picture
		divElement.id = "divID";
		console.log("creating a div");
														// Styling it
		divElement.style.textAlign = "center";
		divElement.style.fontWeight = "bold";
		divElement.style.fontSize = "20px";
		divElement.style.paddingTop = "15px";
		divElement.style.boxShadow="0 4px 8px 0";
		divElement.style.float="right";
		divElement.style.paddingTop="20px";
		divElement.style.paddingBottom="20px";
		divElement.style.paddingLeft="10px";
		divElement.style.paddingRight="10px";
		divElement.style.paddingBottom="10px";
		divElement.style.marginTop="20px";
		divElement.style.marginLeft="10px";
		divElement.style.marginRight="10px"
		divElement.style.marginBottom="10px";
		divElement.style.backgroundColor="white";
		divElement.style.position="relative";
		divElement.style.maxWidth=maxWidth;
		divElement.style.minHeight=minHeight;
		 return divElement;
}


function done() {
	inputValue = document.getElementById("inputElement").value;
	var picture= document.getElementById("pictureBuffer").getElementsByTagName("img");
	
	 // console.log(inputValue.length);
	 // console.log(picture);
	 if(inputValue.length==0&&picture.length==0)
	 {
		 alert("Add an element");
	 }
	 else if(picture.length==0) 
	 { 
		createTileForParagraphOnly("dataList",inputValue);
	 }
	else {		
		var imageUri = picture[0].src;
		createTileForImageAndParagraph("dataList",inputValue,imageUri);
		}
	 addToLocalStorage("dataList",inputValue,imageUri);
	 changeTitleClick();
	 clearElements();
}

	
function addToLocalStorage(key,inputText, imageUri) {
	if(inputText.length!=0||imageUri.length!=0)
	{
		var inputData = new Data(inputText,imageUri);
		if(typeof(Storage)!=="undefined")
		{ 	
			if(localStorage.getItem(key) === null)
			{	imageDataArray[0]=inputData;
				var myJSON = JSON.stringify(imageDataArray); 
				localStorage.setItem(key,myJSON);
			}
			else
			{
				var fetchedData=localStorage.getItem(key);
				var dataArray = JSON.parse(fetchedData);
				//alert("InputData" +inputData);
				//dataArray[dataArray.length]=inputData;
				dataArray.push(inputData);
				var myJSON = JSON.stringify(dataArray); 
				localStorage.setItem(key,myJSON);
			}
		}
	}
	else
		alert("Enter Data")
}


function Data(inputText,imageURL) {
	this.inputText=inputText;
	this.imageURL=imageURL;
}

function createTileForParagraphOnly(pageType,myData) {
		var minHeight="200px";
		var maxWidth="200px";
		 
		var divElement=initializeDiv(minHeight,maxWidth);
		var paragraph = document.createElement("P");
		paragraph.style.wordWrap="break-word";
		divElement.style.minWidth="200px";
		var inputText = document.createTextNode(myData);
		paragraph.appendChild(inputText);
		divElement.appendChild(paragraph);
		var container = document.getElementById("myContainer");			//div is added to container
		var toolbar = createToolbarFor(pageType);
		divElement.appendChild(toolbar);
		container.insertBefore(divElement,container.firstChild);
}

function createTileForImageAndParagraph(pageType,textData,imageURL) {
		var minHeight="200px";
		var maxWidth="200px";
		var divElement=initializeDiv(minHeight,maxWidth);									// Adding a paragraph to it for title
		var paragraph = document.createElement("P");
		paragraph.style.wordWrap="break-word";
		paragraph.style.marginBottom="16px";
		var InputText = document.createTextNode(textData);
		paragraph.appendChild(InputText);
																//adding Image to tiles
		var imageElement = document.createElement("img");
		imageElement.src = imageURL;
		imageElement.style.width="100%";
		divElement.appendChild(imageElement);
		divElement.appendChild(paragraph)
		//both are added to div
		//creating a toolbar
		var toolbar = createToolbarFor(pageType);
		divElement.appendChild(toolbar);
		//Add all to container
		var container = document.getElementById("myContainer");
		container.insertBefore(divElement,container.firstChild);
		//container.appendChild(divElement);
}


function createToolbarFor(item) {
	var pageType=item;
	if(pageType=="dataList"||pageType=="Archive")
	{	var toolbar = document.createElement("div");
		toolbar.classList.add('toolbar');
		toolbar.style.display="table";
		var span1 = document.createElement('span');
		span1.setAttribute('class','glyphicon glyphicon-hand-up glyphicons-tool');
		var span2 = document.createElement('span');
		span2.setAttribute('class','glyphicon glyphicon-user glyphicons-tool');
		var span3 = document.createElement('span');
		span3.setAttribute('class','glyphicon glyphicon-save-file glyphicons-tool');
		span3.setAttribute('onclick','archiveTile(this)');
		var span4 = document.createElement('span');
		span4.setAttribute('class','glyphicon glyphicon-picture glyphicons-tool');
		var span5 = document.createElement('span');
		span5.setAttribute('class','glyphicon glyphicon-trash glyphicons-tool');
		span5.setAttribute('onclick','deleteTile(this)');
		toolbar.appendChild(span1);
		toolbar.appendChild(span2);
		toolbar.appendChild(span3);
		toolbar.appendChild(span4);
		toolbar.appendChild(span5);	
	}
	
	//Toolbar for Trash
	else if (pageType=="Trash")
	{	
		var toolbar = document.createElement("div");
		toolbar.classList.add('toolbar');
		toolbar.style.display="table";
		toolbar.setAttribute('class',"dropdown");
		toolbar.setAttribute('class',"toolbar");
		var span1 = document.createElement('span');
		span1.setAttribute('class','glyphicon glyphicon glyphicon-option-vertical glyphicons-tool dropdown-toggle');
		span1.setAttribute('data-toggle',"dropdown");
		span1.setAttribute('id',"dropdown-list");
		// <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
      // <li role="presentation"><a role="menuitem" tabindex="-1" href="#">HTML</a></li>
      // <li role="presentation"><a role="menuitem" tabindex="-1" href="#">CSS</a></li>
    // </ul>
		var droplist = document.createElement("ul");
		droplist.setAttribute('class',"dropdown-menu");
		droplist.setAttribute('aria-labelledby',"dropdown-list");
		
		var li1 = document.createElement("li");
		li1.setAttribute('role',"menuitem");
		li1.innerHTML="Restore";
		li1.style.fontSize="13px";
		li1.style.paddingLeft="10px";
		li1.style.paddingTop="5px";
		li1.style.paddingBottom="5px";
		li1.setAttribute('onclick','restore(this)');
		var li2 = document.createElement("li");
		li2.setAttribute('role',"menuitem");
		li2.innerHTML="Delete Permanenet";
		li2.style.fontSize="13px";
		li2.style.paddingLeft="10px";
		li2.style.paddingTop="5px";
		li2.style.paddingBottom="5px";
		li2.setAttribute('onclick','deletePermanent(this)');
		droplist.appendChild(li1);
		droplist.appendChild(li2);
		toolbar.appendChild(span1);
		toolbar.appendChild(droplist);
	}
	
	return toolbar;
	
		
}

function loadData() {
	if(typeof(Storage)!=="undefined")
	{
		if(localStorage.getItem("dataList") === null)
		{
			alert("Welcome to GoogleKeep");
		}
		else
		{
			var fetchedData=localStorage.getItem("dataList");
			var dataArray = JSON.parse(fetchedData);
			for (var i = 0; i < dataArray.length; i++) {
			var textData = dataArray[i].inputText;
			var imageData = dataArray[i].imageURL;
			console.log("PAIR " + i + ": " + dataArray[i].inputText);
			// console.log("PAIR " + i + ": " + dataArray[i].imageURL);
		
				if(imageData==undefined) 
				 { 
					createTileForParagraphOnly("dataList",textData);
					
				 }
				else {		
					createTileForImageAndParagraph("dataList",textData,imageData)	
					}
				}
		}
	}
}


function loadDataFromTrash() {
	if(typeof(Storage)!=="undefined")
	{
		if(localStorage.getItem("Trash") === null)
		{
			alert("Nothing to Display");
		}
		else
		{
			var fetchedData=localStorage.getItem("Trash");
			var dataArray = JSON.parse(fetchedData);
			for (var i = 0; i < dataArray.length; i++) {
			var textData = dataArray[i].inputText;
			var imageData = dataArray[i].imageURL;
			console.log("PAIR " + i + ": " + dataArray[i].inputText);
			// console.log("PAIR " + i + ": " + dataArray[i].imageURL);
		
				if(imageData==undefined) 
				 { 
					createTileForParagraphOnly("Trash",textData);
					
				 }
				else {		
					createTileForImageAndParagraph("Trash",textData,imageData)	
					}
				}
		}
	}
}
function loadDataFromArchive() {
	if(typeof(Storage)!=="undefined")
	{
		if(localStorage.getItem("Archive") === null)
		{
			alert("Nothing to Display");
		}
		else
		{
			var fetchedData=localStorage.getItem("Archive");
			var dataArray = JSON.parse(fetchedData);
			for (var i = 0; i < dataArray.length; i++) {
			var textData = dataArray[i].inputText;
			var imageData = dataArray[i].imageURL;
			console.log("PAIR " + i + ": " + dataArray[i].inputText);
			// console.log("PAIR " + i + ": " + dataArray[i].imageURL);
		
				if(imageData==undefined) 
				 { 
					createTileForParagraphOnly("Archive",textData);
					
				 }
				else {		
					createTileForImageAndParagraph("Archive",textData,imageData)	
					}
				}
		}
	}
}


function deleteTile(getelement) {
	var texts = getelement.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.previousSibling.previousSibling.src;
	}
	
	var fetchedData=localStorage.getItem("dataList");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++) {
	var textData = dataArray[i].inputText;
	var imageData = dataArray[i].imageURL;
	if(textData==texts&&imageData==URLofImage)
	{
		dataArray.splice(i,1);
		var myJSON = JSON.stringify(dataArray); 
		//alert(myJSON);
		localStorage.setItem("dataList",myJSON);
		addToLocalStorage("Trash",textData,imageData);
		var deletingNode = getelement.parentNode.parentNode;
		deletingNode.parentNode.removeChild(deletingNode);
		//alert(deletingNode);
	}	
}
}


function archiveTile(getelement) {
	var texts = getelement.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.previousSibling.previousSibling.src;
	}
	
	var fetchedData=localStorage.getItem("dataList");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++) {
	var textData = dataArray[i].inputText;
	var imageData = dataArray[i].imageURL;
	if(textData==texts&&imageData==URLofImage)
	{
		dataArray.splice(i,1);
		var myJSON = JSON.stringify(dataArray); 
		//alert(myJSON);
		localStorage.setItem("dataList",myJSON);
		addToLocalStorage("Archive",textData,imageData);
		var deletingNode = getelement.parentNode.parentNode;
		deletingNode.parentNode.removeChild(deletingNode);
		//alert(deletingNode);
	}	
}
}


function deletePermanent(getelement) {
	//alert(getelement.parentNode.parentNode.parentNode.id);
	var texts = getelement.parentNode.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.parentNode.previousSibling.previousSibling.src;
	}
	
	var fetchedData=localStorage.getItem("Trash");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++) 
	{
		var textData = dataArray[i].inputText;
		var imageData = dataArray[i].imageURL;
		if(textData==texts&&imageData==URLofImage)
			{
			dataArray.splice(i,1);
			var myJSON = JSON.stringify(dataArray); 
			//alert(myJSON);
			localStorage.setItem("Trash",myJSON);
			// addToLocalStorage("dataList",textData,imageData);
			var deletingNode = getelement.parentNode.parentNode.parentNode;
			deletingNode.parentNode.removeChild(deletingNode);
			//alert(deletingNode);
			}
		
	}
}
function restore(getelement) {	
	var texts = getelement.parentNode.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.parentNode.previousSibling.previousSibling.src;
	}
	var fetchedData=localStorage.getItem("Trash");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++)
		{
			var textData = dataArray[i].inputText;
			var imageData = dataArray[i].imageURL;
			if(textData==texts&&imageData==URLofImage)
			{
				dataArray.splice(i,1);
				var myJSON = JSON.stringify(dataArray); 
				//alert(myJSON);
				localStorage.setItem("Trash",myJSON);
				addToLocalStorage("dataList",textData,imageData);
				var deletingNode = getelement.parentNode.parentNode.parentNode;
				deletingNode.parentNode.removeChild(deletingNode);
				alert(deletingNode);
			}	
}
}
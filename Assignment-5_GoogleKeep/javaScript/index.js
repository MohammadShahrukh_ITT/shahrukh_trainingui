var imageUri;
var inputValue;
var imageDataArray=[];
var UpdatingText,UpdatingURL;

//open and close the adding section
function titleClick() {  
	document.getElementById("inputBoxBeforeClick").style.display = "none";
	document.getElementById("inputBoxAfterClick").style.display = "block";
}

function changeTitleClick() {
	document.getElementById("inputBoxBeforeClick").style.display = "table";
	document.getElementById("inputBoxAfterClick").style.display = "none";
}
//to clear the adding section after done and update
function clearElements() {
	document.getElementById("inputElement").value="";
	var element = document.getElementById("pictureBuffer");
	if(element.hasChildNodes())
	{
		var picture=document.getElementById("myspan");
		element.removeChild(picture);
	}
}
//open file browser to select file
function handleFileSelect(evt) {
	
    var files = evt.target.files; // FileList object
	
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
			if(document.getElementById("pictureBuffer").childNodes.length==0)
			{	
          // Render thumbnail.
          var span = document.createElement('span');
		  span.id="myspan"
          span.innerHTML = ['<img class="thumb" id="myImage" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
		 			
			document.getElementById("pictureBuffer").appendChild(span);
			}
			else { 
				document.getElementById("pictureBuffer").firstChild.firstChild.src = e.target.result;
			}
        };
		})(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }
  
function addPicture() {
	document.getElementById("files").addEventListener('change', handleFileSelect, false);
	}
function addImage() {
	addPicture();
	titleClick();
}	
	
function initializeDiv(minHeight,maxWidth)
{
		var divElement = document.createElement("Div");
														//Created a div Element to disply picture
		divElement.id = "divID";
		divElement.setAttribute('class',"grid-item");
		console.log("creating a div");
		divElement.style.maxWidth=maxWidth;
		divElement.style.minHeight=minHeight;
		divElement.setAttribute("ondblclick",'editTiles(this)');
		 return divElement;
}
function popUp(msg) {
	document.getElementById("messege").innerHTML = msg;
	console.log("modal Called");
	//alert(document.getElementById("messege-modal"));
	//document.getElementById("messege-modal").style.display="block";
	$('#messege-modal').modal();
	
}

function done() {
	inputValue = document.getElementById("inputElement").value;
	var picture= document.getElementById("pictureBuffer").getElementsByTagName("img");

	 if(inputValue.length==0&&picture.length==0)
	 {
		 
		 popUp("Add an Element");
	 }
	 else if(picture.length==0) 
	 { //if there is only text
		createTileForParagraphOnly("dataList",getToday(),inputValue);
	 }
	else {		//for both text and Image
		var imageUri = picture[0].src;
		var date = getToday();
		createTileForImageAndParagraph("dataList",date,inputValue,imageUri);
		}
		if(document.getElementById("doneButton").innerHTML =="Done")
		{
			addToLocalStorage("dataList",getToday(),inputValue,imageUri);
		}
		else if(document.getElementById("doneButton").innerHTML == "Update")
		{
			updateToLocalStorage("dataList",inputValue,imageUri,updatingText,updatingURL);
		}
	 document.getElementById("doneButton").innerHTML = "Done";
	 changeTitleClick();
	 clearElements();
}

//Add data to local storage	
function addToLocalStorage(key, date, inputText, imageUri) {
	if(inputText.length!=0||imageUri.length!=0)
	{
		var inputData = new Data(date,inputText,imageUri);
		if(typeof(Storage)!=="undefined")
		{ 	
			if(localStorage.getItem(key) === null)
			{	imageDataArray[0]=inputData;
				var myJSON = JSON.stringify(imageDataArray); 
				localStorage.setItem(key,myJSON);
			}
			else
			{	var fetchedData=localStorage.getItem(key);
				var dataArray = JSON.parse(fetchedData);
				dataArray.push(inputData);
				var myJSON = JSON.stringify(dataArray); 
				localStorage.setItem(key,myJSON);
			}
		}
	}
	else
	popUp("Enter Data");
}


function Data(date,inputText,imageURL) {
	this.date = date;
	this.inputText=inputText;
	this.imageURL=imageURL;	
}

function createTileForParagraphOnly(pageType,date,myData) {
		var minHeight="auto";
		var maxWidth="200px";
		 
		var divElement=initializeDiv(minHeight,maxWidth);
		var dateOfDay= document.createElement("P");
		dateOfDay.style.fontSize="10px";
		dateOfDay.style.fontFamily="cursive";
		dateOfDay.style.position="absolute";
		dateOfDay.style.top="5px";
		dateOfDay.style.right="8px";
		dateOfDay.style.marginBottom="8px";

		var today = document.createTextNode(date);
		dateOfDay.appendChild(today);
		divElement.appendChild(dateOfDay);
		
		var paragraph = document.createElement("P");
		paragraph.style.wordWrap="break-word";
		paragraph.style.fontFamily="fantasy";
		paragraph.style.marginBottom="24px";
		divElement.style.minWidth="100px";
		var inputText = document.createTextNode(myData);
		paragraph.appendChild(inputText);
		divElement.appendChild(paragraph);
		
		var container = document.getElementById("myContainer");			//div is added to container
		var toolbar = createToolbarFor(pageType);//created the toolbar for  type of page
		divElement.appendChild(toolbar);
		container.insertBefore(divElement,container.firstChild);
}

function createTileForImageAndParagraph(pageType,date,textData,imageURL) {
		var minHeight="auto";
		var maxWidth="200px";
		var divElement=initializeDiv(minHeight,maxWidth);	
		var dateOfDay= document.createElement("P");
		dateOfDay.style.fontSize="12px";
		dateOfDay.style.fontFamily="cursive";
		dateOfDay.style.position="absolute";
		dateOfDay.style.top="8px";
		dateOfDay.style.right="10px";

		var today = document.createTextNode(date);
		dateOfDay.appendChild(today);
		divElement.appendChild(dateOfDay);
		
		var paragraph = document.createElement("P");
		paragraph.style.wordWrap="break-word";
		paragraph.style.marginBottom="24px";
		paragraph.style.fontWeight="100";
		paragraph.style.fontSize="16px";
		var InputText = document.createTextNode(textData);
		paragraph.appendChild(InputText);
																//adding Image to tiles
		var imageElement = document.createElement("img");
		imageElement.src = imageURL;
		imageElement.style.width="100%";
		divElement.appendChild(imageElement);
		divElement.appendChild(paragraph)
		//both are added to div
		//creating a toolbar
		var toolbar = createToolbarFor(pageType);
		divElement.appendChild(toolbar);
		//Add all to container
		var container = document.getElementById("myContainer");
		container.insertBefore(divElement,container.firstChild);
}


function createToolbarFor(item) {
	var pageType=item;
	//Toolbar
	if(pageType=="dataList"||pageType=="Archive")
	{	var toolbar = document.createElement("div");
		toolbar.classList.add('toolbar');
		toolbar.style.display="table";
		toolbar.style.borderRadius="5px";
		var span3 = document.createElement('span');
		span3.setAttribute('class','glyphicon glyphicon-save-file glyphicons-tool');
		span3.setAttribute('onclick','archiveTile(this)');
		span3.setAttribute('title','Archive');
		var span5 = document.createElement('span');
		span5.setAttribute('class','glyphicon glyphicon-trash glyphicons-tool');
		span5.setAttribute('onclick','deleteTile(this)');
		span5.setAttribute('title','Delete');
		toolbar.appendChild(span3);
		toolbar.appendChild(span5);	
	}
	
	//Toolbar for Trash
	else if (pageType=="Trash")
	{	
		var toolbar = document.createElement("div");
		toolbar.classList.add('toolbar');
		toolbar.style.display="table";
		toolbar.setAttribute('class',"dropdown");
		toolbar.setAttribute('class',"toolbar");
		var span1 = document.createElement('span');
		span1.setAttribute('class','glyphicon glyphicon glyphicon-option-vertical glyphicons-tool dropdown-toggle');
		span1.setAttribute('data-toggle',"dropdown");
		span1.setAttribute('id',"dropdown-list");
		
		var droplist = document.createElement("ul");
		droplist.setAttribute('class',"dropdown-menu");
		droplist.setAttribute('aria-labelledby',"dropdown-list");
		
		var li1 = document.createElement("li");
		li1.setAttribute('role',"menuitem");
		li1.innerHTML="Restore";
		li1.style.fontSize="13px";
		li1.style.paddingLeft="10px";
		li1.style.paddingTop="5px";
		li1.style.paddingBottom="5px";
		li1.setAttribute('onclick','restore(this)');
		var li2 = document.createElement("li");
		li2.setAttribute('role',"menuitem");
		li2.innerHTML="Delete Permanenet";
		li2.style.fontSize="13px";
		li2.style.paddingLeft="10px";
		li2.style.paddingTop="5px";
		li2.style.paddingBottom="5px";
		li2.setAttribute('onclick','deletePermanent(this)');
		droplist.appendChild(li1);
		droplist.appendChild(li2);
		toolbar.appendChild(span1);
		toolbar.appendChild(droplist);
	}
	
	return toolbar;
	
		
}

function loadData(key) { 
	if(typeof(Storage)!=="undefined")
	{
		if(localStorage.getItem(key) === null)
		{	if(key=="dataList")
				popUp("Welcome to GoogleKeep")
			else if(key=="Trash"||key=="Archive")
				popUp("Nothing to Display")
				
		}
		else
		{
			var fetchedData=localStorage.getItem(key);
			var dataArray = JSON.parse(fetchedData);
			for (var i = 0; i < dataArray.length; i++) {
			var date = dataArray[i].date;
			var textData = dataArray[i].inputText;
			var imageData = dataArray[i].imageURL;		
				if(imageData==undefined) 
				 { 
					createTileForParagraphOnly(key,date,textData);
					
				 }
				else {		
					createTileForImageAndParagraph(key,date,textData,imageData)	
					}
				}
		}
	}
}


function deleteTile(getelement) {
	var texts = getelement.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.previousSibling.previousSibling.src;
	}
	
	var fetchedData=localStorage.getItem("dataList");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++) {
	var date = dataArray[i].date;
	var textData = dataArray[i].inputText;
	var imageData = dataArray[i].imageURL;
	if(textData==texts&&imageData==URLofImage)
	{
		dataArray.splice(i,1);
		var myJSON = JSON.stringify(dataArray); 
		localStorage.setItem("dataList",myJSON);
		addToLocalStorage("Trash",date,textData,imageData);
		var deletingNode = getelement.parentNode.parentNode;
		deletingNode.parentNode.removeChild(deletingNode);
		
	}	
}
}


function archiveTile(getelement) {
	var texts = getelement.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.previousSibling.previousSibling.src;
	}
	
	var fetchedData=localStorage.getItem("dataList");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++) {
	var date = dataArray[i].date;
	var textData = dataArray[i].inputText;
	var imageData = dataArray[i].imageURL;
	if(textData==texts&&imageData==URLofImage)
	{
		dataArray.splice(i,1);
		var myJSON = JSON.stringify(dataArray); 
		localStorage.setItem("dataList",myJSON);
		addToLocalStorage("Archive",date,textData,imageData);
		var deletingNode = getelement.parentNode.parentNode;
		deletingNode.parentNode.removeChild(deletingNode);
	}	
}
}


function deletePermanent(getelement) {
	var texts = getelement.parentNode.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.parentNode.previousSibling.previousSibling.src;
	}
	
	var fetchedData=localStorage.getItem("Trash");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++) 
	{	var date = dataArray[i].date;
		var textData = dataArray[i].inputText;
		var imageData = dataArray[i].imageURL;
		if(textData==texts&&imageData==URLofImage)
			{
			dataArray.splice(i,1);
			var myJSON = JSON.stringify(dataArray); 
			localStorage.setItem("Trash",myJSON);
			var deletingNode = getelement.parentNode.parentNode.parentNode;
			deletingNode.parentNode.removeChild(deletingNode);
			}
	}
}
function restore(getelement) {	
	var texts = getelement.parentNode.parentNode.previousSibling.innerHTML;
	var URLofImage;
	if(getelement.parentNode.parentNode.previousSibling.previousSibling==null)
	{
	URLofImage=undefined;
	}
	else {
		URLofImage=getelement.parentNode.parentNode.previousSibling.previousSibling.src;
	}
	var fetchedData=localStorage.getItem("Trash");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++)
		{	var date = dataArray[i].date;
			var textData = dataArray[i].inputText;
			var imageData = dataArray[i].imageURL;
			if(textData==texts&&imageData==URLofImage)
			{
				dataArray.splice(i,1);
				var myJSON = JSON.stringify(dataArray); 
				localStorage.setItem("Trash",myJSON);
				addToLocalStorage("dataList",getToday(),textData,imageData);
				var deletingNode = getelement.parentNode.parentNode.parentNode;
				deletingNode.parentNode.removeChild(deletingNode);
			}	
}
}

var isSideBarOpen = 1
function showSideBar(){
	var mySideBar = document.getElementById('sidebar');
	if(!isSideBarOpen){
		mySideBar.style.display = "block";
		mySideBar.style.opacity="1";
		mySideBar.position="absolute";
		mySideBar.style.left="0px";
		mySideBar.style.trasition="visibility 0s, opacity 0.3s, left 0.3s"
		//mySideBar.style.width = "16%";
		isSideBarOpen =1;
	}
	else {
		isSideBarOpen = 0;
		mySideBar.style.left="400px";
		mySideBar.style.opacity="0";
		mySideBar.visibility="hidden";
	}
}
function editTiles(getElement) {
	var texts="",URLofImage;
	
	if(getElement.childNodes.length==4)
	{
		URLofImage = getElement.childNodes[1].src;
		texts = getElement.childNodes[2].innerHTML;
		var span = document.createElement('span');
		  span.id="myspan"
          span.innerHTML = ['<img class="thumb" id="myImage" src="', URLofImage,
                             '"/>'].join('');
		 			
		document.getElementById("pictureBuffer").appendChild(span);
	}
	else if(getElement.childNodes.length==3)
	{ 
	texts = getElement.childNodes[1].innerHTML;
		
	}
	titleClick();
	
		document.getElementById("inputElement").value=texts;
		document.getElementById("doneButton").innerHTML = "Update";
		updatingText = texts;
		updatingURL = URLofImage;
		getElement.parentNode.removeChild(getElement);
}
function updateToLocalStorage(key,inputValue,imageUri,updatingText,updatingURL) {
	var fetchedData=localStorage.getItem("dataList");
	var dataArray = JSON.parse(fetchedData);
	for (var i = 0; i < dataArray.length; i++) 
	{	var date = getToday();
		var textData = dataArray[i].inputText;
		var imageData = dataArray[i].imageURL;
		if(textData==updatingText&&imageData==updatingURL)
			{console.log("updating");
			dataArray[i].date = date;
			dataArray[i].inputText=inputValue;
			dataArray[i].imageURL = imageUri;
			var myJSON = JSON.stringify(dataArray); 
			localStorage.setItem("dataList",myJSON);
			}
		
	}
}
function getToday() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	var today = dd+'/'+mm+'/'+yyyy;
	return today;
}
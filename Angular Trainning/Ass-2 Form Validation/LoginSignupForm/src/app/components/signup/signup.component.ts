import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
 //onsubmit method perform ather the click on Register
 onSubmit(user:any ) {
     this.checkUser(user);
     }

 //checkUser method check the user in database
 checkUser(user:any) {
     if(JSON.parse(localStorage.getItem(user.email))!= null)
     {
       alert("your email already exist");
     }
     else
     {
       this.addUser(user);  
     }  
  }
 //add User method add user in the localstorage
 addUser(user:any) {
     if (localStorage)
     {       
         localStorage.setItem(user.email,JSON.stringify(user));
         this.router.navigate(['/login']);    
     }
     else
     {
         alert("Sorry, your browser does not support Web Storage");
     }    



 }
}


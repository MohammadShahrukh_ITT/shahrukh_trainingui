import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../Service/local-storage.service';
import { UserService } from '../../Service/user.service';
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  ngOnInit() {
  }
  public userFirstName;
  public userLastName;
  public userEmail;
  constructor(private route: ActivatedRoute, private router:Router) {
    let email = this.route.snapshot.params['user'];
    let user = JSON.parse(localStorage.getItem(email));
    this.userFirstName=user.Name.fname;
    this.userLastName=user.Name.lname;
    this.userEmail = user.email;  
  }
  
logout() {
  this.router.navigate(['/login']);
}
}

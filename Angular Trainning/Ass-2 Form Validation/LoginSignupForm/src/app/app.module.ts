import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    WelcomeComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
   RouterModule.forRoot([
   { path:'', component:SignupComponent },
   { path:'signup', component:SignupComponent },
   { path:'login', component:LoginComponent },
   { path:'welcome/:user', component:WelcomeComponent }
     ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}

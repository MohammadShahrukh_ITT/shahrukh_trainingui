import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageServiceService {

 //set Local Storage Data
SetLocalStorateData(key:string,data:any[]) {
      localStorage.setItem(key,JSON.stringify(data));
    }   

//get local storage data
getLocalStorateData(key:string):any[] {    
return JSON.parse(localStorage.getItem(key));
    
}
}

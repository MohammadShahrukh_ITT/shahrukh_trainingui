import { Injectable } from '@angular/core';

@Injectable()
export class PostServiceService {
  postDataArray:any[]=[];
  commentDataArray:any[]=[];
  getPostData():any[] {
    return this.postDataArray;
  }
  setPostData(postDataArray,postDataInfo) {
    if(postDataArray==null) {
      this.postDataArray.unshift(postDataInfo);
    }
    else {
    this.postDataArray = postDataArray;
    this.postDataArray.unshift(postDataInfo);
    }
  }
  getcommentData():any[] {
    return this.commentDataArray;
  }
  setcommentData(postDataArrayofComments, postId, commentInfo) {
    var postInfo;
    for(let data of postDataArrayofComments) {
      if(data.PostIndex == postId) {
        postInfo = data;
        var comments = data.Comments;
         comments.push(commentInfo);
         data.Comments = comments;
         this.postDataArray = postDataArrayofComments;
         
       }
     }
  }
 
  likeDislikePost(postDataArray, postId, currentUser) {
    for(let data of postDataArray) {
      if(data.PostIndex == postId) {
          if(data.likes.length == 0) {
            var newLike = {
              like:true,
              likerName : localStorage.getItem("currentUser"),
              postId:postId,
            }
            data.likes.push(newLike);
            data.noOfLikes = data.noOfLikes+1;
            this.postDataArray = postDataArray;
          }
         else { var flag = false;
           for (let likeData of data.likes ) {
             if(likeData.postId == postId && likeData.likerName == currentUser) { 
               if (likeData.like == true) {
                data.noOfLikes = data.noOfLikes-1;
                likeData.like = false;
               }
               else { 
                data.noOfLikes = data.noOfLikes+1;
                likeData.like = true;
               }
               this.postDataArray = postDataArray;
               flag=true;
             }
             
           }
           if(flag==false) {
              var newLike = {
                like:true,
                likerName : localStorage.getItem("currentUser"),
                postId:postId,
              }
              data.likes.push(newLike);
              data.noOfLikes = data.noOfLikes+1;
              this.postDataArray = postDataArray;
            } 
         }
       }
     }
  }
  checkLike(postDataArray,currentUser,postId) {
    for(let data of this.postDataArray) {
      if(data.PostIndex == postId) { 
        for (let likeData of data.likes ) {
          if(likeData.postId == postId && likeData.likerName == currentUser) { 
            if (likeData.like == true) {
               return true;
            }
            else if (likeData.like == false) {
              return false;
           }
          }
        }
      }
    }
  }
}

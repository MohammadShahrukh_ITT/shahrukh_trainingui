import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FbHeaderComponent } from './Components/fb-header/fb-header.component';
import { PostContainerComponent } from './Components/post-container/post-container.component';
import { ModalDirective, ModalModule } from 'ngx-bootstrap';
import { FileUploaderComponent } from './Components/file-uploader/file-uploader.component';
import { ModalDialogComponent } from './Components/modal-dialog/modal-dialog.component';
import { ModalImageUploadComponent } from './Components/modal-image-upload/modal-image-upload.component';
import { LoginComponent } from './Components/login/login.component';
import { SignupComponent } from './Components/signup/signup.component';
import { LoginSignupComponent } from './Components/login-signup/login-signup.component';
import { FormsModule } from '@angular/forms';
import { PostComponent } from './Components/post/post.component';


@NgModule({
  declarations: [
    AppComponent,
    FbHeaderComponent,
    PostContainerComponent,
    ModalDialogComponent,
    FileUploaderComponent,
    ModalImageUploadComponent,
    LoginComponent,
    SignupComponent,
    LoginSignupComponent,
    PostComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forRoot([
      { path:'head', component:FbHeaderComponent },
      { path:'fb/:user', component:PostContainerComponent },
      { path:'', component:LoginSignupComponent },
      { path:'modal',component:ModalDialogComponent },
      { path:'fileUpload',component:FileUploaderComponent},
      { path:'imageUpload', component:ModalImageUploadComponent},
      { path:'login',component:LoginComponent},
      { path:'signup',component:SignupComponent},
      { path:'loginSignup', component:LoginSignupComponent},
      { path:'post', component:PostComponent }
      
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

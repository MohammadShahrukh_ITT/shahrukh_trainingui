import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-fb-header',
  templateUrl: './fb-header.component.html',
  styleUrls: ['./fb-header.component.css']
})
export class FbHeaderComponent implements OnInit {

  constructor(private router: Router) { }
  userName =this.getUserName();
  profileUrl ='/assets/logo1.jpg';
  ngOnInit() {
  }
logOut() {
  localStorage.setItem("currentUser",null);
  this.router.navigate(['/loginSignup']);
}
getUserName() {
  let cur_user = localStorage.getItem("currentUser");
  let cur_user_data = JSON.parse(localStorage.getItem(cur_user));
  return cur_user_data.first_name +" "+ cur_user_data.last_name;
}
}

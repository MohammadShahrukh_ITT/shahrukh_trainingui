import { Component, Input, ViewChild, OnInit, EventEmitter, Output } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';
import { LocalStorageServiceService } from '../../Services/local-storage-service.service';
import { PostServiceService } from '../../Services/post-service.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-modal-image-upload',
  templateUrl: './modal-image-upload.component.html',
  styleUrls: ['./modal-image-upload.component.css'],
  providers: [LocalStorageServiceService, PostServiceService]
})
export class ModalImageUploadComponent {
  @ViewChild('imageUploadModal') public imageUploadModal: ModalDirective;
  @Output()
  imageUrl1: EventEmitter<string> = new EventEmitter();
  url;
  postdata;
  postDataList:any[];
  posts:any;
  key:string="posts";
  static indexOfPost = Math.floor(Math.random() * (999999 - 10)) + 10;
  @Input() title?: string;
  constructor(private route: ActivatedRoute, private postService: PostServiceService, private storage: LocalStorageServiceService) {
    this.postDataList = this.postService.getPostData();;
  }
  show() {
    this.imageUploadModal.show();
  }
  hide() {
    this.imageUploadModal.hide();
  }
  postData(value) {
    this.postdata = {
      PostIndex:ModalImageUploadComponent.indexOfPost,
      userName:this.getUserName(),
      profileURL:'/assets/logo1.jpg',
      postText:value,
      imageUrl:this.url,
      noOfLikes:0,
      noOfComments:13,
      likes:[],
      Comments:[],
      date:new Date().getTime()
    } 
    
   if(this.url==undefined) {
     alert("Upload an Image");
     return;
    }
    this.postDataList = this.storage.getLocalStorateData(this.key);
    this.postService.setPostData(this.postDataList,this.postdata);
    this.postDataList = this.postService.getPostData();
    console.log("From postDataList");
    console.log(this.postDataList);
    this.storage.SetLocalStorateData(this.key, this.postDataList);

    console.log("after set");
    console.log(this.postDataList);
     this.cleanUp();
  }
  ImageUrl(value) {
    this.url = value;
  }
  cleanUp() {
    this.postData=null;
    this.url=null;
    this.imageUploadModal.hide();
  }
  ngOnInit() {

  }
  getUserName() {
    let cur_user = localStorage.getItem("currentUser");
    let cur_user_data = JSON.parse(localStorage.getItem(cur_user));
    return cur_user_data.first_name +" "+ cur_user_data.last_name;
  }

}





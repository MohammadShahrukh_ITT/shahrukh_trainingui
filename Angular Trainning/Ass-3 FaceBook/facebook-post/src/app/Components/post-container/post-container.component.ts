import { Component, ViewChild, NgModule,ViewContainerRef,OnInit,ElementRef } from '@angular/core';
import { ModalDirective,ModalModule } from 'ngx-bootstrap';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';
import { ModalDialogComponent } from '../modal-dialog/modal-dialog.component';
import { ModalImageUploadComponent } from '../modal-image-upload/modal-image-upload.component';
import { PostComponent } from "../post/post.component";
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-post-container',
  templateUrl: './post-container.component.html',
  styleUrls: ['./post-container.component.css'],
  
  
})
export class PostContainerComponent implements OnInit {
  @ViewChild('popupModal') popupModal:ModalDialogComponent;
  @ViewChild('imageUploadModal') imageUploadModal:ModalImageUploadComponent;
  @ViewChild('imageUploadModal') post:PostComponent;
  userName;
  constructor(private route: ActivatedRoute, private router:Router) { 
    let email = this.route.snapshot.params['user'];
    let user = JSON.parse(localStorage.getItem(email));
    this.userName = user.first_name;
  }

  ngOnInit() {
   
  }

}

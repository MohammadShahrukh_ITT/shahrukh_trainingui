import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { LocalStorageServiceService } from '../../Services/local-storage-service.service';
import { PostServiceService } from '../../Services/post-service.service';
import { ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  providers:[LocalStorageServiceService, PostServiceService]
})
export class PostComponent implements OnInit {
  key="posts";
  commentData;
  newComment;
  postDataList;
  commentDataList;
  postID;
  currUser;
  varShowHide;
   
  constructor(private route: ActivatedRoute, private postService:PostServiceService, private storage:LocalStorageServiceService, private cdr: ChangeDetectorRef ) {
    this.varShowHide=true;
  }

  ngOnInit() {
    this.postDataList = this.storage.getLocalStorateData(this.key);
    console.log("From Local Storage");
    console.log(this.postDataList);
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }


  likeDislike(postId) {
    this.postDataList = this.storage.getLocalStorateData(this.key);
    this.currUser = localStorage.getItem("currentUser");
    this.postService.likeDislikePost(this.postDataList, postId, this.currUser);
    this.postDataList = this.postService.getPostData();
    this.storage.SetLocalStorateData(this.key, this.postDataList);
  }

  getUserName() {
    let cur_user = localStorage.getItem("currentUser");
    let cur_user_data = JSON.parse(localStorage.getItem(cur_user));
    return cur_user_data.first_name +" "+ cur_user_data.last_name;
  }

  comment(commentText,postId) {
    this.newComment = {
      comment:commentText,
      commenterName :this.getUserName(),
      profileUrl:'/assets/logo1.jpg'
    }
   
    this.postDataList = this.storage.getLocalStorateData(this.key);
    console.log(this.postDataList);
    this.postService.setcommentData(this.postDataList, postId, this.newComment);
    this.postDataList = this.postService.getPostData();
    this.storage.SetLocalStorateData(this.key, this.postDataList);   
  }
  showHide() {
    return this.varShowHide;
  }
  changeShowHide() {
    this.varShowHide = !this.varShowHide;
  }
  check (postId) {
    this.postDataList = this.storage.getLocalStorateData(this.key);
    this.currUser = localStorage.getItem("currentUser");
    return this.postService.checkLike(this.postDataList, this.currUser, postId);
  }
 getTime(value) {
   var ms = new Date().getTime()-value;
   var hrs = Math.floor((ms/1000/60/60) << 0);
   var min = Math.floor((ms/1000/60) << 0);
   var seconds = Math.floor((ms/1000) % 60);
   if(hrs>0) {
     return hrs +" hours ago";
   }
   else if(min>0) {
     return min + " min ago";
        }
      else {
        return "Just now";
      }
 }
}

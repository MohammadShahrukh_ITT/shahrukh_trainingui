import {Component} from 'angular2/core';
import { EventEmitter} from 'angular2/core'
@Component({
    selector:'my-tutorials',
    template:`<h2>Child Component</h2>
                <label>Enter chiolld component value</label>
                <input type = "text" #ctext (keyup)="onChange(ctext.value)">
                <p>The value of parent Component in child component is:</p>
                 {{parentData}}`,
                inputs:[`parentData`],
                outputs:['childEvent']

}) 
export class TutorialComponent{
  
public parentData;
childEvent=new EventEmitter<String>();
onChange(data) {
this.childEvent.emit(data);
}
}
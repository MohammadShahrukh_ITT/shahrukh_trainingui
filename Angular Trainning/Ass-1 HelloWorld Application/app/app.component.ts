import {Component} from 'angular2/core';
import {TutorialComponent} from './tutorial.component'
@Component({
    selector: 'my-app',
    template: `<h1>Premt App component</h1>
                <label>Enter parent component value</label>
                <input type="text" #ptext (keyup)="0">
                <p>The Value of Child Componenet in parent is: {{childData}}</p>
                <my-tutorials (childEvent)="childData=$event" [parentData]="ptext.value"></my-tutorials>`,
    directives: [TutorialComponent],
    
})
export class AppComponent { 
childData:String;
}
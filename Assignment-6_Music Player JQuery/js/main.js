$(document).ready(function(){
 
var audio;

// Loading the data from AJAX file

	$.getJSON( "JSON/data.json", function( data ) {
	var items = [];
	$.each( data, function( key, val ) { 
	items.push('<li id ="'+ key + '" song = "'+ val.song +'"cover="'+val.cover+'" artist="'+val.artist+'" >'+key+ ". "+val.song +'</li>');
	});
  
	$("#playlist").append(items);  
	initAudio($('#playlist li:first-child'));
	});
	
//Hide Pause
$('#pause').hide();

function initAudio(element){

	var song = element.attr('song');
	var title = element.text();
	var cover = element.attr('cover');
	var artist = element.attr('artist');
	
	//Create audio object
	audio = new Audio('media/'+ song);
	
	//Insert audio info
	$('.artist').text(artist);
	$('.title').text(title);
	
	//Insert song cover
	$('img.cover').attr('src','images/covers/'+cover);
	
	$('#playlist li').removeClass('active');
	element.addClass('active');
}

//Play button
$('#play').click(function(){
	audio.play();
	$('#play').hide();
	$('#pause').show();
	showDuration();
});

//Pause button
$('#pause').click(function(){
	audio.pause();
	$('#play').show();
	$('#pause').hide();
});

//Stop button
$('#stop').click(function(){
	audio.pause();
	audio.currentTime = 0;
});

//Next button
$('#next').click(function(){
	audio.pause();
	var next = $('#playlist li.active').next();
	if(next.length == 0){
		next = $('#playlist li:first-child');
	}
	initAudio(next);
	audio.play();
	showDuration();
});

//Prev button
$('#prev').click(function(){
	audio.pause();
	var prev = $('#playlist li.active').prev();
	if(prev.length == 0){
		prev = $('#playlist li:last-child');
	}
	initAudio(prev);
	audio.play();
	showDuration();
});

//Playlist song click
$(document).on("click", "#playlist", function() {
    console.log('li.click: ' +$(this).text());
});


$('#playlist li').click(function(){
	alert("clicked");
	audio.pause();
	initAudio($(this));
	$('#play').hide();
	$('#pause').show();
	audio.play();
	showDuration();
});


//Time/Duration
function showDuration(){
	$(audio).bind('timeupdate',function(){
		//Get hours and minutes
		var s = parseInt(audio.currentTime % 60);
		var m = parseInt(audio.currentTime / 60) % 60;
		var ts = parseInt(audio.duration % 60);
		var tm = parseInt(audio.duration / 60) % 60;
		if(s < 10){
			s = '0'+s;
		}
		$('#duration').html(m + ':'+ s);
		$('#totalDuration').html(tm + ':'+ ts);
		
		var value = 0;
		if(audio.currentTime > 0){
			value = Math.floor((100 / audio.duration) * audio.currentTime);
			console.log(value);
		}
		$('#progress').css('width',value+'%');
		console.log(value);
	});
}


});


	

var person = /** @class */ (function () {
    function person(firstName, lastName, emailAddress, contact, user_password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.contact = contact;
        this.user_password = user_password;
    }
    return person;
}());
var employee = /** @class */ (function () {
    function employee() {
    }
    employee.prototype.submit = function () {
        if (typeof (Storage) !== "undefined") {
            var firstName = document.getElementById("firstName").value;
            var lastName = document.getElementById("lastName").value;
            var emailAddress = document.getElementById("email").value;
            var contact = document.getElementById("contact").value;
            var user_password = document.getElementById("password").value;
            var emp = new person(firstName, lastName, emailAddress, parseInt(contact), user_password);
            if (!this.validate(emp)) {
                return;
            }
            var myJSON = JSON.stringify(emp);
            localStorage.setItem(emailAddress, myJSON);
            window.location.assign("index.html");
        }
    };
    employee.prototype.validate = function (person) {
        if (person.firstName.trim(" ") == 0 || person.lastName.trim(" ") == 0) {
            alert("First Name and Last Name fields can not be blank");
            return false;
        }
        if (this.hasNumbers(person.firstName) || this.hasNumbers(person.lastName)
            || this.hasSpecialSymbol(person.firstName) || this.hasSpecialSymbol(person.lastName)) {
            alert("First Name and Last Name Cannot contain Numbers and special Symbol");
            return false;
        }
        if (!this.validateEmail(person.emailAddress)) {
            alert("enter a valid email Address");
            return false;
        }
        if (!this.validatePhone(person.contact)) {
            alert("Enter a valid Phone no");
            return false;
        }
        if (!this.validatePassword(person.user_password)) {
            alert("Enter Valid Password");
            return false;
        }
        return true;
    };
    employee.prototype.hasNumbers = function (name) {
        return /\d/.test(name);
    };
    employee.prototype.hasSpecialSymbol = function (name) {
        return /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(name);
    };
    employee.prototype.validateEmail = function (email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    };
    employee.prototype.validatePhone = function (phone) {
        var phoneno = /^\d{10}$/;
        return phoneno.test(phone);
    };
    employee.prototype.validatePassword = function (pass) {
        var passw = /^[A-Za-z]\w{7,14}$/;
        return passw.test(pass);
    };
    employee.prototype.login = function () {
        if (typeof (Storage) !== "undefined") {
            var employee_1 = localStorage.getItem("Kmoshahrukh@gmail.com");
            alert(employee_1);
            var jsonObj = JSON.parse(employee_1);
            var Email = jsonObj.emailAddress;
            var pass = jsonObj.user_password;
            if ((Email == document.getElementById("emailAddress").value) && (pass == document.getElementById("password").value)) {
                alert("Hello");
                localStorage.setItem("currentUser", Email);
                window.location.assign("Welcome.html");
            }
            else {
                alert("Incorrect User Name or password");
            }
        }
    };
    return employee;
}());
function loginValidation() {
    var logIn = new employee();
    logIn.login();
}
function submitFunction() {
    console.log("submit data");
    var submit = new employee();
    submit.submit();
}
function welcome() {
    if (typeof (Storage) !== "undefined") {
        var currentUser = localStorage.getItem("currentUser");
        var employee = localStorage.getItem(currentUser);
        var jsonObj = JSON.parse(employee);
        document.getElementById("name").innerHTML = "Hello " + jsonObj.firstName + " " + jsonObj.lastName;
    }
}
function logout() {
    localStorage.setItem("currentUser", null);
    window.location.href = "index.html";
}

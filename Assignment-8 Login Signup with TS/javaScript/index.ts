class person {
    firstName : string;
    lastName : string;
    emailAddress : string;
    contact : Number;
    user_password : string;
    
     constructor(firstName : string, lastName : string, emailAddress : string, contact : Number, user_password :string) {
         this.firstName = firstName;
         this.lastName = lastName;
         this.emailAddress = emailAddress;
         this.contact = contact;
         this.user_password = user_password;
         
        }
}

class employee {
     
    submit() {
       
        if(typeof(Storage)!=="undefined") {
            let firstName = (<HTMLInputElement>document.getElementById("firstName")).value
            let lastName = (<HTMLInputElement>document.getElementById("lastName")).value;
            let emailAddress = (<HTMLInputElement>document.getElementById("email")).value;
            let contact = (<HTMLInputElement>document.getElementById("contact")).value;
            let user_password = (<HTMLInputElement>document.getElementById("password")).value;
            let emp = new person(firstName,lastName,emailAddress,parseInt(contact),user_password);
            if(!this.validate(emp)) { 
                return;
                }
            var myJSON = JSON.stringify(emp);
            localStorage.setItem(emailAddress, myJSON);
            window.location.assign("index.html");
            }
    
    }
    validate(person)
    { if(person.firstName.trim(" ")==0||person.lastName.trim(" ")==0)
            {
                alert("First Name and Last Name fields can not be blank");
                return false;
            }
     
        if(this.hasNumbers(person.firstName)||this.hasNumbers(person.lastName)
            ||this.hasSpecialSymbol(person.firstName)||this.hasSpecialSymbol(person.lastName)) {
               alert("First Name and Last Name Cannot contain Numbers and special Symbol");
                return false;
           } 
        if(!this.validateEmail(person.emailAddress)) {  
            alert("enter a valid email Address");
            return false; 
            }
        if(!this.validatePhone(person.contact)) { 
            alert("Enter a valid Phone no");
            return false;
        }
        if(!this.validatePassword(person.user_password)) {
            alert("Enter Valid Password");
            return false;
        }
        return true;
    }
    
    hasNumbers(name):boolean
    {
        return /\d/.test(name);
    }
    
    hasSpecialSymbol(name):boolean
    {
       return /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(name); 
    }
    
    validateEmail(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
    
    validatePhone(phone) {
        var phoneno = /^\d{10}$/;  
        return phoneno.test(phone); 
        
    }
    
    validatePassword(pass) {
        let passw=  /^[A-Za-z]\w{7,14}$/;  
        return passw.test(pass);  
    }
    
    login() { 
			if(typeof(Storage)!=="undefined") {
				let employee = localStorage.getItem("Kmoshahrukh@gmail.com");
                alert(employee);
				let jsonObj = JSON.parse(employee);
				let Email = jsonObj.emailAddress;
				let pass = jsonObj.user_password;
                
				
				if ((Email == (<HTMLInputElement>document.getElementById("emailAddress")).value) && ( pass == (<HTMLInputElement>document.getElementById("password")).value))
					{ alert("Hello");
					localStorage.setItem("currentUser",Email);
					window.location.assign("Welcome.html");
					}
					else
					{ alert("Incorrect User Name or password")}
				}
			}  
    
}
function loginValidation() {
    var logIn  = new employee();
    logIn.login();
}
function submitFunction() {
    console.log("submit data");
    var submit  = new employee();
    submit.submit();
}
function welcome() {
    if (typeof(Storage) !== "undefined") {
       var currentUser = localStorage.getItem("currentUser");
       var employee = localStorage.getItem(currentUser);
        var jsonObj = JSON.parse(employee);
		document.getElementById("name").innerHTML= "Hello "+jsonObj.firstName+" " + jsonObj.lastName;
		}
}
function logout() {
		localStorage.setItem("currentUser",null);
		window.location.href="index.html";
		}

		
		

        export interface calculator {
            input_exp:string;
            addition(x: number,y: number):number;
            subtraction(x: number,y: number):number;
            multiplication(x: number,y: number):number;
            division(x: number,y: number):number;
            modulous(x: number,y: number):number;
            evaluate(expression:string):number;
        }
       
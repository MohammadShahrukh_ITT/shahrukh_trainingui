var Calculator = /** @class */ (function () {
    function Calculator() {
    }
    Calculator.prototype.addition = function (x, y) {
        return x + y;
    };
    Calculator.prototype.subtraction = function (x, y) {
        return x - y;
    };
    Calculator.prototype.multiplication = function (x, y) {
        return x * y;
    };
    Calculator.prototype.division = function (x, y) {
        return x / y;
    };
    Calculator.prototype.modulous = function (x, y) {
        return x % y;
    };
    Calculator.prototype.evaluate = function (expression) {
        var res = expression.split(" ");
        var i, result = Number(res[0]);
        for (i = 1; i < res.length; i++) {
            if (res[i] == "+") {
                console.log(result);
                result = this.addition(result, Number(res[++i]));
            }
            else if (res[i] == "-") {
                console.log(result);
                result = this.subtraction(result, Number(res[++i]));
            }
            else if (res[i] == "*") {
                console.log(result);
                result = this.multiplication(result, Number(res[++i]));
            }
            else if (res[i] == "/") {
                console.log(result);
                result = this.division(result, Number(res[++i]));
            }
            else if (res[i] == "%") {
                console.log(result);
                result = this.modulous(result, Number(res[++i]));
            }
        }
        return result;
    };
    return Calculator;
}());
function input_click_calc(input_id) {
    "use strict";
    var my_text = new Calculator();
    var input_text = my_text.input_exp;
    input_text = document.getElementById("calc-screen").value + input_id;
    console.log(input_text);
    document.getElementById("calc-screen").value = String(input_text);
}
function operations_calc(input_id) {
    "use strict";
    var my_text = new Calculator();
    var input_text = my_text.input_exp;
    input_text = document.getElementById("calc-screen").value + " " + input_id + " ";
    console.log(input_text);
    document.getElementById("calc-screen").value = String(input_text);
}
function results_calc(input_id) {
    "use strict";
    var my_text = new Calculator();
    var input_text = my_text.input_exp;
    console.log(input_id);
    switch (input_id) {
        case "C":
            input_text = "";
            document.getElementById("calc-screen").value = String(input_text);
            break;
        case "arrow":
            var data = document.getElementById("calc-screen").value;
            document.getElementById("calc-screen").value = data.substring(0, data.length - 1);
            break;
        case "=":
            var str = document.getElementById("calc-screen").value;
            console.log(str);
            var eval_result = new Calculator();
            var final_result = eval_result.evaluate(str);
            console.log(final_result);
            document.getElementById("calc-screen").value = String(final_result);
            break;
    }
}
